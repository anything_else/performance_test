﻿using System;
using System.Diagnostics;

namespace Performance
{
    sealed public class StringComp
    {
        private const string _str1 = "DupaJasiuKaruzela";
        private const string _str2 = "dUpAJAsiukaruzela";
        private readonly Stopwatch _sw;

        public StringComp()
        {
            _sw = new Stopwatch();
        }

        public void ConsoleLogAll()
        {
            Console.WriteLine("\n==============================");
            Console.WriteLine($"Comparing strings: {_str1} {_str2}");
            Console.WriteLine("==============================");
            Console.WriteLine($"Equals: {Equals()}");
            Console.WriteLine($"Equals ToUpper(): {EqualsToUpper()}");
            Console.WriteLine($"Equals ToLower(): {EqualsToLower()}");

            Console.WriteLine($"Compare Default: {CompareDefault()}");
            Console.WriteLine($"Compare IgnoreCase: {CompareIgnoreCase()}");
        }

        public long Equals()
        {
            _sw.Restart();
            if(_str1 == _str2) {             
                
            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long EqualsToUpper()
        {
            _sw.Restart();
            if (_str1.ToUpper() == _str2.ToUpper())
            {

            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long EqualsToLower()
        {
            _sw.Restart();
            if (_str1.ToLower() == _str2.ToLower())
            {

            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long CompareDefault()
        {
            _sw.Restart();
            string.Compare(_str2, _str1);
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long CompareIgnoreCase()
        {
            _sw.Restart();
            string.Compare(_str2, _str1, StringComparison.InvariantCultureIgnoreCase);
            _sw.Stop();

            return _sw.ElapsedTicks;
        }
    }
}
