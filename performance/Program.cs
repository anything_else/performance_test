﻿
using System;

namespace Performance
{
    class Program
    {
        static void Main(string[] args)
        {
            var itr = new Iteration(100000);
            itr.ConsoleLogAll();

            var str = new StringComp();
            str.ConsoleLogAll();

            Console.ReadLine();
        }
    }
}
