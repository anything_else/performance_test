﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Performance
{
    sealed public class Iteration
    {
        private readonly int[] _coll;
        private readonly int _cnt;
        private readonly Stopwatch _sw;

        public void ConsoleLogAll()
        {
            Console.WriteLine("\n==============================");
            Console.WriteLine("Iteration speed");
            Console.WriteLine("==============================");
            Console.WriteLine($"For on array {MeasureFor()}");
            Console.WriteLine($"foreach on array: {MeasureForEachOnArray()}");
            Console.WriteLine($"forach on IEnumerable<T>: {MeasureForEachOnIEnum()}");

            Console.WriteLine($"for on List<T>: {MeasureForOnList()}");
            Console.WriteLine($"forach on List<T>: {MeasureForEachOnList()}");

            Console.WriteLine($"for on List<T> object: {MeasureForOnListObject()}");
            Console.WriteLine($"forach on List<T> object: {MeasureForEachOnListObject()}");
        }

        public Iteration(int cnt)
        {
            _coll = new int[cnt];
            _cnt = cnt;

            for (int i = 0; i < cnt; i++)
            {
                _coll[i] = i;
            }

            _sw = new Stopwatch();
        }


        public long MeasureFor()
        {
            int sum = 0;

            _sw.Restart();
            for (int i = 0; i <_cnt; i++)
            {
                sum += _coll[i];
            }
            _sw.Stop();


            return _sw.ElapsedTicks;
        }

        public long MeasureForEachOnIEnum()
        {
            int sum = 0;
            IEnumerable<int> temp = _coll;

            _sw.Restart();
            foreach (var item in temp)
            {
                sum += item;
            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long MeasureForEachOnArray()
        {
            int sum = 0;

            _sw.Restart();
            foreach (var item in _coll)
            {
                sum += item;
            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long MeasureForEachOnList()
        {
            int sum = 0;
            var temp = new List<int>(_coll);

            _sw.Restart();
            foreach (var item in temp)
            {
                sum += item;
            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        public long MeasureForOnList()
        {
            int sum = 0;
            var temp = new List<int>(_coll);

            _sw.Restart();
            for (int i = 0; i < _cnt; i++)
            {
                sum += temp[i];
            }
            _sw.Stop();

            return _sw.ElapsedTicks;
        }

        private List<DumbObject> SetupDumbList()
        {
            var res = new List<DumbObject>(_cnt);
                
            for(int i = 0; i < _cnt; i++)
            {
                res.Add(new DumbObject {
                    String = "this is stupid string",
                    String2 = "even more on the stupid side of strings",
                    Int1 = 3,
                    Int2 = 5
                });
            }

            return res;
        }

            public long MeasureForEachOnListObject()
            {
                var targets = SetupDumbList();

                _sw.Restart();
                foreach (var item in targets)
                {
                    var r  = $"{item.Int1} {item.Int2} {item.String} {item.String2}";
                }
                _sw.Stop();

                return _sw.ElapsedTicks;
            }

            public long MeasureForOnListObject()
            {
               var targets = SetupDumbList();

                _sw.Restart();
                for (int i = 0; i < targets.Count; i++)
                {
                    var r  = $"{targets[i].Int1} {targets[i].Int2} {targets[i].String} {targets[i].String2}";
                }
                _sw.Stop();

                return _sw.ElapsedTicks;
            }

    }

    public class DumbObject 
    {
        public string String { get; set; }
        public int Int1 { get; set; }
        public string  String2 { get; set; }
        public int Int2 { get; set; }
    }
}
